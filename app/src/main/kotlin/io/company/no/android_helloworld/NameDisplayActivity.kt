package io.company.no.android_helloworld

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView

class NameDisplayActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_name_display)

        val intent = this.intent

        val textView = findViewById(R.id.textView) as TextView
        val pattern = resources.getText(R.string.text_view_content) as String

        textView.text = String.format(pattern, intent.getStringExtra(MainActivity.INTENT_MESSAGE_NAME))
    }
}
