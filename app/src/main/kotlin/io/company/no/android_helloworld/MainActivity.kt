package io.company.no.android_helloworld

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import io.company.no.android_helloworld.R

class MainActivity : AppCompatActivity() {

    companion object {
        val INTENT_MESSAGE_NAME = "intent_message_name"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun showReportPage(v: View) {
        val txtName = findViewById(R.id.txtName) as EditText
        val lblErrorMessage = findViewById(R.id.lblErrorMessage) as TextView
        val name = txtName.text.toString().trim()
        if (name.isBlank()) {
            lblErrorMessage.text = resources.getText(R.string.no_input_warning)
        } else {
            lblErrorMessage.text = ""
            val intent = Intent(this, NameDisplayActivity::class.java)
            intent.putExtra(INTENT_MESSAGE_NAME, name)
            startActivity(intent)
        }
    }
}
